#!/bin/bash
# assumes that the repo is in the same dir as the joomla install
# ./<repo>
# ./<joomla>/<joomla files>
CWD=`pwd`
cd ../
# sync the site
echo "lib Sync  [sp4k]"
if [ ! -d "./joomla/libraries/sp4k" ]; then
	mkdir "./joomla/libraries/sp4k"
fi
rsync -a $CWD/ ./joomla/libraries/sp4k --exclude-from "$CWD/sync_exclude.txt"